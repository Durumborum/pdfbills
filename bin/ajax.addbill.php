<?php

    require '../vendor/autoload.php';
    
    use Bill\PDFBills;

    $PDFBills = new PDFBills();
    
sleep(1);

$response = new StdClass();

if ( empty($_POST['firma'] ) ) {
    $response->result = 'error';
    $response->required = ['firma'];
} else {
    $PDFBills->make_bill($_POST['firma']);
    $response->result = 'success';
}

header( 'Content-Type:application/json');
echo json_encode($response);

