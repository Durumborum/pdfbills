<?php

namespace Bill;

use Dompdf\Dompdf;
use Dompdf\Options;
use \PDO;

class PDFBills {

    public function __construct() {
        $this->db = new PDO(
            'mysql:host=localhost;dbname=bill',
            'root',
            ''
        );
    }

public function get_all_bills() {
    
    $sql = "SELECT * FROM `bills`";

    $result = $this->db->query( $sql );
    $all = $result->fetchAll(PDO::FETCH_CLASS);
    return $all;
}

public function get_next_billnumber() {

    $sql = "SELECT rnr FROM `bills` ORDER BY rnr DESC LIMIT 1";

    $result = $this->db->query( $sql );
    $number = $result->fetchAll(PDO::FETCH_CLASS);

    if ( count($number ) > 0 ) {
        $lastnr = $number[0]->rnr;
    
        $number = (int) substr($lastnr, 1);
        
        $nextnumber = sprintf("%04d", $number+1);

        return 'R' . $nextnumber;
        
    } else {
        return 'R0001';
    }
}

public function insert_bill_db( $filename, $firma, $billnumber ) {

    $sql = "
        INSERT INTO bills (rnr,filename,date,firma)
        VALUES (:billnumber,:filename,:date,:firma)
    ";

    $args = array(
        'billnumber' => $billnumber,
        'filename' => $filename,
        'date' => date('Y-m-d H:i:s', time() ),
        'firma' => $firma,
    );

    return $this->db->prepare($sql)->execute($args );
}

function make_bill($firma) {
    // set the font through the Options class
    $options = new Options();
    
    $options->set('defaultFont', 'Helvetica');
    
    // instantiate and use the dompdf class
    $dompdf = new Dompdf( $options );
    
    $next_nr = $this->get_next_billnumber();
    
    $filename = dirname(__FILE__) . '/../bin/template/bill_template.html';
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));
    //var_dump($contents);
    
    $contents = preg_replace(
        array( '/%Rechnungsnummer%/'),
        array( $next_nr ),
        $contents
    );
    
    fclose($handle);
    
    $dompdf->loadHtml($contents);
    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'portrait');
    // Render the HTML as PDF
    $dompdf->render();
    
    $pdf_gen = $dompdf->output();
    $bill_filename = 'bill'.time().'.pdf';
    
    if(file_put_contents('../public/pdfs/'.$bill_filename, $pdf_gen)){
        // insert in DB
        $this->insert_bill_db($bill_filename, $firma, $next_nr);
    }
}
}