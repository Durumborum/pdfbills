function load_bills() {
    $.ajax({
        url:'../bin/ajax.loadbills.php',
        success:function( response ) {
            $('#billtable-loader').hide();
            for(let i in response.bills) {
                $('<tr>')
                    .append( $('<td>').html( response.bills[i].rnr) )
                    .append( $('<td>').append(
                        $('<a>')
                            .attr('href','pdfs/' +  response.bills[i].filename)
                            .attr( 'target', '_blank')
                            .html('Open PDF')
                    ) )
                    .append( $('<td>').html( response.bills[i].firma) )
                    .append( $('<td>').html('Not paid') )
                    .appendTo( '#bills')
            } 
        }
    });
}

function add_bill() {
    $.ajax({
        url:'../bin/ajax.addbill.php',
        method:'post',
        data:{ firma: $('#firma').val() },
        success:function(response) {
            if ( response.result == 'error' ) {
                alert('here')
                for ( let i in response.required ) {
                    $( '#' + response.required[i] ).addClass( 'is-invalid' );
                }  } 
                else {
                    $('#billtable-loader').show();
                    $('#bills tr + tr').remove()
                    $('#firma').val('').removeClass( 'is-invalid' );
                    load_bills();
            }
        }
    });
}

$( document ).ready( function() {
    load_bills();
});

$( document ).on('submit', '#newbill', function(e) {
    e.preventDefault();
    add_bill();
});

$( document ).on('change keyup', 'input', function(e) {
    
});

