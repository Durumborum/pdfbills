<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Bills</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mystyle.css">
    
</head>
<body>
    
    <div class="container">
    <h1>PDFBills</h1>
    <table class="table table-striped" >
    <thead>
        <tr>
            <th scope="col">Bill #</th>
            <th scope="col">Bill as PDF</th>
            <th scope="col">Firma</th>
            <th scope="col">Status</th>
        </tr>
    </thead>
    <tbody id="bills">
            <tr id="billtable-loader">
                <td colspan="4">
                    <div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status"> 
                            <span class="sr-only">Loading...</span>
                        </div>    
                    </div>
                </td>
            </tr>
    </tbody>
    </table >
    <h2>New Bill</h2>
    
    <form action="" id="newbill">
        <div class="form-group">
            
            <label for="firma">Firma:</label> 
            <input id="firma" class="form-control" placeholder="Firma name" type="text">
        </div>
        <button id="submitbill" class="btn btn-primary">Submit bill</button>
    </form>
    </div>


 
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/myscript.js"></script>
</body>
</html>